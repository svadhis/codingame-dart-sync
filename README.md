# Codingame Dart Sync

Template to sync from local IDE to Codingame IDE.

Files have to be part of the library code which is the main.dart file. 

*DON'T REMOVE THE `// PARTS` COMMENT IN THE MAIN FILE*

In Codingame Sync App, choose code.dart file. (https://www.codingame.com/forum/t/codingame-sync-beta/614)

To start watching for changes and compile, run the command: 
```
dart compile.dart
```