
import 'dart:io';

void compile() {
  File codeInput = File('./lib/main.dart');
  File codeOutput = File('./code.dart');

  List<String> codeText = codeInput.readAsLinesSync();

  List<String> parts = codeText.where((line) => line.contains("part '")).toList();

  parts = parts.map((part) {
    String filePath = part.split("'")[1];
    File partFile = File('./lib/' + filePath);

    return partFile.readAsStringSync().replaceAll('part of code;', '// PART FILE $filePath');
  }).toList();

  codeText.removeWhere((line) => line.contains("part '"));

  codeText.insertAll(codeText.indexOf('// PARTS') + 1, parts);

  codeOutput.writeAsStringSync(codeText.join('\n'));
}

main() {
  File lib = File('./lib');

  Stream<FileSystemEvent> watchOutput = lib.watch(events: FileSystemEvent.modify, recursive: true);

  watchOutput.listen(
    (event) {
      compile();
      print('Compiled files into code.dart');
    }, 
    onError: (e) => print(e)
  );

  print('Listening to changes in lib folder...');
}
